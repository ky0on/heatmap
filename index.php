<?php
//@usage access to 'index.php?data=33.5363,-117.044,30,33.5608,-117.24,20'

//init
$debug = false;
$data = '[';   // $data = '[{lat: 33.5363, lng:-117.044, count: 1},...]'
$max = 50;     // see map.js
$clat = 35.737019;     // center of map
$clng = 139.540100;   // center of map

//check GET DATA - data
if (isset($_GET['data'])){
	$tmp = rtrim($_GET['data'], ',');   // remove last ',' if end with ','
	$getdata = split(',', $tmp);
} else {
	$debug = true;
	$getdata = array(35.727366, 139.53925, 40, 35.7273, 139.5392, 20, 35.7271, 139.5391, 30);
}

//check GET DATA
if (isset($_GET['max'])) $max = $_GET['max'];
if (isset($_GET['clat'])) $clat = $_GET['clat'];
if (isset($_GET['clng'])) $clng = $_GET['clng'];

//generate data array for heatmap
for($i=0; $i<count($getdata); $i+=3) {
	$data .= "{lat: {$getdata[$i]}, lng: {$getdata[$i+1]}, count: {$getdata[$i+2]}}";
	if ($i != count($getdata)-3) $data .= ', ';
}
$data .= ']';

//debug
/* var_dump($getdata); */
/* var_dump($data); */
?>



<!-- html -->
<html>
  <head>  <!-- head -->
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <title>Map</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- css -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- js -->
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
  </head>

  <body> <!-- body -->
    <div id="main">
	  <?php if ($debug) echo('<h1>[Debug mode]</h1>'); ?>
      <div id="heatmapArea">
      </div>
    </div>
    <script type="text/javascript" src="./js/heatmap.js"></script>
    <script type="text/javascript" src="./js/heatmap-gmaps.js"></script>
    <script type="text/javascript" src="./js/map.js"></script>
    <script>
	  var clat = <?php echo $clat ?>;
	  var clng = <?php echo $clng ?>;
      initMap();
      var data = <?php echo $data ?>;
      var max = <?php echo $max ?>;
      drawHeatmap(data, max);
    </script>
  </body>
</html>
