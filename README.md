# Heatmap

## Usage
* GET
    * ?data: lat1,lng1,cnt1,...,latN,lngN,cntN
    * ?max: max value of cnt  
    * ?clat: center of map (lat)
    * ?clng: center of map (lng)
* e.g. http://park.itc.u-tokyo.ac.jp/nino-lab/Heatmap/index.php?data=35.727366,139.53925,40,35.7273,139.5392,20,35.7271,139.5391,30
