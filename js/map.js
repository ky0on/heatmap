var map;
var heatmap; 

initMap = function(){
    //init
    var myLatlng = new google.maps.LatLng(clat, clng);

    var myOptions = {
	zoom: 13,
	center: myLatlng,
	mapTypeId: google.maps.MapTypeId.ROADMAP,
	disableDefaultUI: false,
	scrollwheel: true,
	draggable: true,
	navigationControl: true,
	mapTypeControl: false,
	scaleControl: true,
	disableDoubleClickZoom: false
    };
    map = new google.maps.Map(document.getElementById("heatmapArea"), myOptions);
    
    heatmap = new HeatmapOverlay(map, {"radius":25, "visible":true, "opacity":60});
    
    // document.getElementById("tog").onclick = function(){
    // 	heatmap.toggle();
    // };
};

drawHeatmap = function(data, max) {
    // alert(data);
    // console.log(data);

    if (data != null) {
	var plotData={
    	    max: max,
	    data: data
	};
    } else{
	var plotData={
    	    max: max,
	    data: [{lat:35.727366, lng:139.53925, count:40}, {lat:35.7273, lng:139.5392, count:20}, {lat:35.7271, lng:139.5391, count:30}]
	};
    }    
    console.log(plotData);
    
    // this is important, because if you set the data set too early, the latlng/pixel projection doesn't work
    google.maps.event.addListenerOnce(map, "idle", function(){
    	heatmap.setDataSet(plotData);
    });
};
